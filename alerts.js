/*
 * Copyright (C) 2013 Seldszar <http://www.seldszar.fr/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

var EventEmitter = require("events").EventEmitter;
var util = require("util");

var Alerts = function() {
    this.list = [];
    this.last = null;
}
util.inherits(Alerts, EventEmitter);

(function() {

    this.parse = function(str) {
        var pattern = /([\w-]+)\s+\((.*)\): (.*) - (\d+)m - (\d+)cr(?: - (.*)\s+\((.*)\))?/;
        if (!pattern.test(str.text))
            return null;
        var matches = pattern.exec(str.text);
        var id = str.id;
        var duration = parseInt(matches[4]);
        var credits = parseInt(matches[5]);
        var beginDate = this.parseDate(str.created_at);
        var endDate = new Date(beginDate);
        endDate.setMinutes(endDate.getMinutes() + duration);
        var alert = {
            "id": id,
            "location": matches[1],
            "planet": matches[2],
            "mission": matches[3],
            "begin": beginDate,
            "end": endDate,
            "credits": credits
        };
        if (matches[6]) {
            alert["reward"] = {
                "name": matches[6],
                "type": matches[7]
            };
        }
        return alert;
    };

    this.add = function(alert) {
        alert = this.parse(alert);
        if (alert && !this.exists(alert.id)) {
            this.list.unshift(alert);
            this.last = alert;
            this.emit("add", alert);
        }
        return alert;
    };

    this.addMany = function(str) {
        var self = this;
        str.forEach(function(s) {
            self.add(s);
        });
    };

    this.sort = function() {
        this.list = this.list.sort(function(a, b) {
            if (a.begin < b.begin)
                return 1;
            if (a.begin > b.begin)
                return -1;
            return 0;
        });
    };

    this.parseDate = function(date) {
        return new Date(Date.parse(date.replace(/( \+)/, " UTC$1")));
    }

    this.exists = function(id) {
        for (var i = 0; i < this.list.length; ++i) {
            if (this.list[i].id == id)
                return true;
        }
        return false;
    }

}).call(Alerts.prototype);

module.exports = new Alerts;
