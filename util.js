/*
 * Copyright (C) 2013 Seldszar <http://www.seldszar.fr/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

exports.extend = require("extend");

/**
 * Convert a string to a boolean
 *
 * @param string str Boolean string
 * @return boolean Boolean value
 */
function parseBool(str) {
    return str == "true" || str == "1";
}
exports.parseBool = parseBool;

/**
 * Check if an object is undefined
 *
 * @param object obj Object to check
 * @return boolean True if the object is undefined ; otherwise false
 */
function isUndefined(obj) {
    return obj === undefined;
}
exports.isUndefined = isUndefined;

/**
 * Check if an object is defined
 *
 * @param object obj Object to check
 * @return boolean True if the object is defined ; otherwise false
 */
function isDefined(obj) {
    return !isUndefined(obj);
}
exports.isDefined = isDefined;
